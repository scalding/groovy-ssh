import org.apache.sshd.SshServer
import org.apache.sshd.server.Command
import org.apache.sshd.server.Environment
import org.apache.sshd.server.ExitCallback
import org.apache.sshd.server.PasswordAuthenticator
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider
import org.apache.sshd.server.session.ServerSession
import org.apache.sshd.server.shell.ProcessShellFactory
import org.codehaus.groovy.tools.shell.Groovysh
import org.codehaus.groovy.tools.shell.IO

class GroovyShellCommand implements Command {
    InputStream inputStream
    OutputStream outputStream
    OutputStream errorStream
    ExitCallback exitCallback

    EnumSet<ProcessShellFactory.TtyOptions> ttyOptions

    @Override
    void start(Environment env) throws IOException {
        def i = new TtyFilterInputStream(inputStream)
        def o = new TtyFilterOutputStream(outputStream)
        def e = new TtyFilterOutputStream(errorStream)
        def p = new PrintStream(o)
        [i, o, e].each {
            it.ttyOptions = ttyOptions
        }
        def io = new IO(i, o, e)

        def binding = new Binding().with {
            setVariable("inputStream", i)
            setVariable("outputStream", o)
            setVariable("errorStream", e)
            setVariable("out", p)
            setVariable("println", p.&println)
            return it
        }
        def shell = new Groovysh(binding, io)
        Thread.start {
            def code = shell.run([] as String[])
            if (exitCallback) {
                exitCallback.onExit(code)
            }
        }
    }

    @Override
    void destroy() {
    }
}

class GroovyShellFactory implements org.apache.sshd.common.Factory<Command> {
    EnumSet<ProcessShellFactory.TtyOptions> ttyOptions

    @Override
    Command create() {
        return new GroovyShellCommand(ttyOptions: ttyOptions)
    }
}

def sshd = SshServer.setUpDefaultServer().with {
    port = 2222
    shellFactory = new GroovyShellFactory(ttyOptions: EnumSet.of(ProcessShellFactory.TtyOptions.ONlCr))

    passwordAuthenticator = new PasswordAuthenticator() {
        @Override
        boolean authenticate(String username, String password, ServerSession session) {
            username == 'jj' && password == 'testme'
        }
    }

    keyPairProvider = new SimpleGeneratorHostKeyProvider("/tmp/hostkey.ser")

    return it
}
sshd.start()
Thread.sleep(Long.MAX_VALUE)